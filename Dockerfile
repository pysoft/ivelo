# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set environment variables


# Set the working directory
WORKDIR /build

# Install dependencies
COPY requirements.txt /build/
RUN pip install -r requirements.txt

# Copy the project code into the container
COPY . /build/