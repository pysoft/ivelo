import uproot
import numpy as np
import os
import requests
import json
#import shutil 


def _post(apath, data):
    session = requests.Session()
    session.trust_env = False
    content = session.post(
        "{}".format('http://10.128.124.243:8003/api/' + apath),
        data = json.dumps(data),
        verify = False,
    )
    content.raise_for_status()
    return content.json()

def setload(run, setname, catalog, entity):

    base2run = int(np.floor(run / 10000.0) * 10000)
    base3run = int(np.floor(run / 1000.0) * 1000)
    
    set = uproot.open(f'/hist/Savesets/ByRun/{setname}/{base2run}/{base3run}/VeloMon-run{run}.root')[catalog][entity]

    set = set.to_numpy()
    #print(set)
    return set

def saveset(set, run, cat, dir, name):
    #storage = f'/calib/velo/.runbase/{cat}/'
    ###
    #if not os.path.isdir(storage):
    #    os.mkdir(storage)
    #if not os.path.isdir(storage + str(run)):
    #    os.mkdir(storage + str(run))
    #if not os.path.isdir(storage + str(run) + '/' + str(dir)):
    #    os.mkdir(storage + str(run) + '/' + str(dir))
    ######
    #print(set[0].astype(int))

    #set_json = set[0].to_list()

    #np.savetxt(f'{storage}{run}/{dir}/{name}.csv', set[0], fmt = '%i', delimiter = ' ', newline = ' ')
    json_set = {}
    #print(set[0])
    for i in range(624):
        json_set[i] = set[0][i]

    #print(json_set)

    data = {'runid': run,
            'uploaded_by': os.getlogin(),
            'uploaded_from': os.uname().nodename,}
            #'path_perasic_nobeam':f'{storage}{run}/{dir}/{name}.csv',}
        
    #POST the run recipe
    _post('clusters', data)    

    data = {'runid': run,
            'set': json_set}
            #'path_perasic_nobeam':f'{storage}{run}/{dir}/{name}.csv',}
        
    #POST the little ones
    _post('clustersasic', data)   

    return None