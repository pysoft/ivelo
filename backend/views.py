from django.shortcuts import render, redirect
from django.contrib import auth
from rest_framework.authtoken.models import Token
from django.http import JsonResponse
#from .utils import get_current_git_commit

from bokeh.plotting import figure, save, output_file
from bokeh.embed import components
from bokeh.models import Quad, NumericInput, LinearColorMapper, ColumnDataSource, Select, CustomJS, Slider
from bokeh.models import LogScale, LinearScale, ColorBar, RangeSlider, Slope
from bokeh.layouts import column, row
from bokeh.palettes import diverging_palette, Sunset10

from bokeh.transform import linear_cmap
from bokeh.util.hex import hexbin

from backend.setloader import setload, saveset
#from bokeh.models.widgets import RangeSlider
import numpy as np
from copy import copy

from backend.api import get

from datetime import datetime
import requests

def getChipRecipe(request):
    data = {'Hm': 'Hmm'}
    print(request)
    kind = request.GET.get('kind', None)
    asic = request.GET.get('asic', None)
    date = request.GET.get('date', None)
    date = datetime.strptime(date, '%H:%M:%S %d-%m-%Y')
    date = date.strftime('%Y-%m-%d-%H-%M-%S')
    print(kind, asic, date)
    asic = int(asic)
    print(asic)
    mod, tile, asic = int(asic/12), int((asic%12)/3), (asic%12)%3
    print(mod, tile, asic)
    fpath = ('/calib/velo/.recipe/VeloPix/Chip/' + kind + '/' + date + '/Module' + str(mod).zfill(2) + '/Module' + 
              str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '.dat')
    with open(fpath, 'r') as file:
        data = file.read()
    print(data)
    return JsonResponse(data, safe = False)

def velofactorize(vpid):
    return int(vpid/12), int((vpid%12)/3), int((vpid%12)%3)

def _get(call):
    session = requests.Session()
    session.trust_env = False
    content = session.get(
        "{}{}".format('http://10.128.124.243:8003', call),
            verify = False,
    )
    content.raise_for_status()
    return content.json()

def main(request):
    token = None
    if request.user.is_authenticated:
        token = Token.objects.get(user=request.user)
    #context = {'commit_hash': get_current_git_commit(), 'token': token, 'user': request.user}
    return render(request, 'main/index.html')#, {'script':None, 'div':None})

# savesets manager
def get_saveset(request):
    runid = int(request.GET.get('runid'))
    set = setload(runid, "VeloMon", "VPClusterMonitors_NoBeam", "VPClustersPerASIC")
    #print(set)
    if 1:
        saveset(set, runid, "VeloMon", "VPClusterMonitors_NoBeam", "VPClustersPerASIC")
    return JsonResponse('', safe = False)

# data reader manager
def get_data(request):
    kind = request.GET.get('kind')
    runid = request.GET.get('runid')
    if kind == 'runnoise':
        #return get_data_runnoise(runid);
        return JsonResponse('', safe = False)

def get_data_runnoise(runid):
    item = _get(f'/api/clusters?run={runid}')
    print(item)
    return item


# bokeh manager
def get_bokeh(request):
    cat = request.POST.get('cat', '')
    vpid = request.POST.get('vpid', '')
    dbid = request.POST.get('dbid', '')
    view = request.POST.get('view', '')
    type = request.POST.get('from', '')
    #print(request)

    if cat == 'Equalisation':
        get_bokeh_equalisation(vpid, dbid, view, type)
    elif cat == 'Time alignment':
        get_bokeh_ta(vpid, dbid, view)
    elif cat == 'Matrix':
        get_bokeh_matrix(vpid, dbid, view)
    #if response == 1:
    #    return render(request, 'main/bokeh.html', {})
    else:
        print('here before')
        output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    return render(request, 'main/bokeh.html', {})

# bokeh submanager for equalisation
def get_bokeh_equalisation(vpid, dbid, view, type):
    if view == 'masks':
        get_bokeh_masks(vpid, dbid)
    if view == 'thr_in_e':
        get_bokeh_thrine(vpid, dbid)
    if view == 'thr_in_e_2d':
        get_bokeh_thrine2d(vpid, dbid)
    elif view == 'fine: nstd':
        get_bokeh_nstd(vpid, dbid, type, 'scan16')
    elif view == '0x0: nstd':
        get_bokeh_nstd(vpid, dbid, type, 'scan0')
    elif view == '0xF: nstd':
        get_bokeh_nstd(vpid, dbid, type, 'scan15')
    elif view == 'fine: nmean':
        get_bokeh_noisemap(vpid, dbid, type, 'scan16', property = 'mean', binsize = 2)
    elif view == '0x0: nmean':
        get_bokeh_noisemap(vpid, dbid, type, 'scan0', property = 'mean')
    elif view == '0xF: nmean':
        get_bokeh_noisemap(vpid, dbid, type, 'scan15', property = 'mean')
    elif view == 'fine: nrate':
        get_bokeh_noisemap(vpid, dbid, type, 'scan16', property = 'rate', binsize = 2)
    elif view == '0x0: nrate':
        get_bokeh_noisemap(vpid, dbid, type, 'scan0', property = 'rate')
    elif view == '0xF: nrate':
        get_bokeh_noisemap(vpid, dbid, type, 'scan15', property = 'rate')
    elif view == 'trim':
        get_bokeh_trim(vpid, dbid)
    elif view == 'trim (hist)':
        get_bokeh_trim_hist(vpid, dbid)
    elif view == '0x0: nstd (hist)':
        get_bokeh_nstd_hist(vpid, dbid, type, 'scan0')
    elif view == '0xF: nstd (hist)':
        get_bokeh_nstd_hist(vpid, dbid, type, 'scan15')
    elif view == 'fine: nstd (hist)':
        get_bokeh_nstd_hist(vpid, dbid, type, 'scan16')

def get_bokeh_ta(vpid, dbid, view):

    print('Bokeh manager for TA running on...')
    print(vpid, dbid, view)
    if view == 'bxid_mean' or view == 'incr. TA':
        get_bokeh_tamean(vpid, dbid)
    elif view == 'TA vs baseline':
        get_bokeh_ta_vs_baseline(vpid, dbid)

def get_bokeh_matrix(vpid, dbid, view):
    print('Bokeh manager for Matrix recipe running on...')
    print(vpid, dbid, view)
    if view == 'masks':
        get_bokeh_matrix_masks(vpid, dbid)

# bokeh tasks for time alignment
def get_bokeh_tamean(vpid, dbid):
    mod, tile, asic = velofactorize(int(vpid))
    
    #send GET request to the db
    fpath = _get('/api/ta/custom?type=asic&dbid=' +dbid+ '&vpid=' +vpid+ '&param=csv_path')['csv_path']

    datamap = np.loadtxt(fpath)
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255), 
               background_fill_color= '#FFFFFF', border_fill_color = '#FFFFFF')
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    c_mapper = LinearColorMapper(palette = 'Turbo256')
    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)

    cb_low = RangeSlider(start=-5, end=6, value= (-5,6), 
        step=0.05, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=110, x_range = (-5,6), y_range = (1, 360000), y_axis_type = 'log')
    bins = np.linspace(-5.5, 5.5, 12)
    hist, edges = np.histogram(datamap, bins=bins)
    hist = np.where(hist != 0, hist, 1)
    #print(hist)
    src_hist = ColumnDataSource(dict(x=edges[:-1], y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist, mode = 'after')
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'BXID - BXID ref'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = -5, 6
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 1, top = 360000, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)

    fpath_bxid_hist = _get('/api/ta/custom?type=asic&dbid=' +dbid+ '&vpid=' +vpid+ '&param=bxid_hist_path')['bxid_hist_path']
    hist_bxid_full = np.loadtxt(fpath_bxid_hist)
    hist_bxid_full = np.where(hist_bxid_full != 0, hist_bxid_full, 1)
    hist_bxid_full = np.append(hist_bxid_full, 1)
    #graph_bxid = figure(width=220, height=110, x_range = (-5,6), y_range = (1, 60000), y_axis_type = 'log')
    #bins = np.linspace(-5.5, 7.5, 14)
    #hist_bxid_full, edges = np.histogram(datamap, bins=bins)
    #hist = np.where(hist != 0, hist, 1)
    #print(hist)
    src_hist_bxid = ColumnDataSource(dict(x=edges[:-1], y=hist_bxid_full))
    #print(edges[:-1], hist_bxid_full, hist)
    graph.step(x = 'x', y = 'y', line_color = '#FF0000', source = src_hist_bxid, mode = 'after')
    #graph.yaxis.visible = False
    #graph.xaxis.axis_label_text_font_size = '8pt'
    #graph.xaxis.axis_label = 'BXID - BXID ref'

    _common_setup_mainplot(p)
    cb = r.construct_color_bar(width = 15, background_fill_color = '#FFFFFF')
    p.add_layout(cb, 'right')
    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch() )
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

# bokeh tasks for time alignment
def get_bokeh_ta_vs_baseline(vpid, dbid):
    mod, tile, asic = velofactorize(int(vpid))
    
    #send GET request to the db
    fpath = _get('/api/ta/custom?type=asic&dbid=' +dbid+ '&vpid=' +vpid+ '&param=csv_path')['csv_path']
    datamap_ta = np.genfromtxt(fpath, delimiter = ' ').ravel()
    #print(fpath, datamap_ta, datamap_ta.shape)

    eqid = _get('/api/ta/custom?dbid=' +dbid+ '&param=equalisation')['equalisation']
    #print(eqid)
    print(eqid)
    fpath_baseline = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=path')['path']
    threshold = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=globalthreshold')['globalthreshold']
    fpath_baseline += 'Module' + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '_scan16_mean.csv'
    datamap_eq = np.genfromtxt(fpath_baseline, delimiter = ',').ravel() 
    print(datamap_eq, threshold)
    datamap_eq = threshold - datamap_eq
    print(datamap_eq)
    #print(datamap_ta - datamap_eq)
    #print(np.where(datamap_ta != np.nan, datamap_ta - datamap_eq, np.nan))


    p = figure(width=445, height=340, #y_range = (-4,5), #x_range = (1400,1700), y_range = (-5, 5), 
               background_fill_color= '#FFFFFF', border_fill_color = '#FFFFFF')
    

    x = datamap_eq[np.isnan(datamap_ta) == False] 
    y = datamap_ta[np.isnan(datamap_ta) == False]
    x = x[x > 1]
    y = y[x > 1]
    #p.scatter(x, y, size = 5, alpha = 0.15, fill_color = "red", line_color = None)

    #test hex hist
    bins = hexbin(x, y, 0.2)
    H, xedges, yedges = np.histogram2d(x, y, bins = 40)
    source = ColumnDataSource(dict(
        left = xedges,
        top = yedges,
        right = xedges + 0.1,
        bottom = yedges - 0.1,
        counts = H,
    ))
    p.hex_tile(q="q", r = "r", size = 0.2, line_color = None, source = bins, fill_color = linear_cmap('counts', 'Viridis256', 0, max(bins.counts)))
    #p.quad(left = 'left', right = 'right', top = 'top', bottom = 'bottom', line_color = None, source = source, 
    #       fill_color = linear_cmap('counts', 'Viridis256', 0, max(H.ravel())))
    fit = np.polyfit(x, y , 1)

    p.xaxis.axis_label_text_font_size = '8pt'
    p.xaxis.axis_label = 'Threshold - noise baseline [DAC]'
    p.yaxis.axis_label_text_font_size = '8pt'
    p.yaxis.axis_label = 'BXID spread'
    #print(datamap_ta[not np.isnan(datamap_ta )])
    print(fit)
    #slope = Slope(gradient=fit[0], y_intercept=fit[1],
    #          line_color="blue", line_dash='dashed', line_width=4)
    #p.add_layout(slope)
    #sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    #src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    #c_mapper = LinearColorMapper(palette = 'Turbo256')
    #r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)

    #cb_low = RangeSlider(start=-5, end=6, value= (-5,6), 
    #    step=0.05, title="Colorbar scale", width = 160, show_value = True)

    #cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    #cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    #graph = figure(width=220, height=110, x_range = (-5,6), y_range = (1, 60000), y_axis_type = 'log')
    #bins = np.linspace(-5.5, 5.5, 12)
    #hist, edges = np.histogram(datamap, bins=bins)
    #hist = np.where(hist != 0, hist, 1)
    #print(hist)
    #src_hist = ColumnDataSource(dict(x=edges[:-1], y=hist))
    #graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist, mode = 'after')
    #graph.yaxis.visible = False
    #graph.xaxis.axis_label_text_font_size = '8pt'
    #graph.xaxis.axis_label = 'BXID - BXID ref'
    #print(graph.x_range.start, graph.y_range.end)
    #l_x, r_x = -5, 6
    #l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    #l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    #r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    #l_box = Quad(left = 'l_x', right = 'r_x', bottom = 1, top = 60000, fill_color = 'blue', fill_alpha = 0.05)
    #graph.add_glyph(l_src, l_box)
    #callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    #cb_low.js_on_change('value', callback_range)

    #fpath_bxid_hist = _get('ta', 'asic', vpid, dbid, 'bxid_hist_path')['bxid_hist_path']
    #hist_bxid_full = np.loadtxt(fpath_bxid_hist)
    #hist_bxid_full = np.where(hist_bxid_full != 0, hist_bxid_full, 1)
    #hist_bxid_full = np.append(hist_bxid_full, 1)
    #graph_bxid = figure(width=220, height=110, x_range = (-5,6), y_range = (1, 60000), y_axis_type = 'log')
    #bins = np.linspace(-5.5, 7.5, 14)
    #hist_bxid_full, edges = np.histogram(datamap, bins=bins)
    #hist = np.where(hist != 0, hist, 1)
    #print(hist)
    #src_hist_bxid = ColumnDataSource(dict(x=edges[:-1], y=hist_bxid_full))
    #print(edges[:-1], hist_bxid_full, hist)
    #graph.step(x = 'x', y = 'y', line_color = '#FF0000', source = src_hist_bxid, mode = 'after')
    #graph.yaxis.visible = False
    #graph.xaxis.axis_label_text_font_size = '8pt'
    #graph.xaxis.axis_label = 'BXID - BXID ref'

    #_common_setup_mainplot(p)
    #cb = r.construct_color_bar(width = 15, background_fill_color = '#FFFFFF')
    #p.add_layout(cb, 'right')
    #callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch() )
    #sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p))

# bokeh tasks for matrix recipe
def get_bokeh_matrix_masks(vpid, dbid) -> None:
    mod, tile, asic = velofactorize(int(vpid))

    #send GET request to the db
    fpath = _get('/api/recipe/subrecipe/matrix?id=' +dbid+ '&asic=' +vpid+ '&param=fpath')['fpath']

    #datamap = np.loadtxt(fpath)
    #print(datamap)

    f = open(fpath, 'r')
    line = f.readline().split('=')[1]
    masks = np.zeros([256, 256])
    #accumulated: int = 0
    for col in range(256):
        #mask1, mask2 = 0, 0
        maskline = np.zeros([256])
        for i in range(128):
            chunk = line[i*3: i*3 +3]
            #print(chunk[1], format(int(chunk[1], 16), '04b'))
            
            mask1 = int(format(int(chunk[0], 16), '04b')[1])
            mask2 = int(format(int(chunk[1], 16), '04b')[3])
            maskline[i*2] = mask1
            maskline[i*2 + 1] = mask2
            #print(i, chunk, chunk[0], chunk[1], mask1, mask2)
        masks[:, col] = maskline
        line = f.readline().split('=')[-1]
        
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255), 
               background_fill_color= '#FFFFFF', border_fill_color = '#FFFFFF')
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [masks], image_const = [masks]))
    c_mapper = LinearColorMapper(palette = 'Cividis256')
    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)

    _common_setup_mainplot(p)
    cb = r.construct_color_bar(width = 15, background_fill_color = '#FFFFFF')
    p.add_layout(cb, 'right')
    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch() )
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, width = 200)))

# bokeh tasks for equalisation
def get_bokeh_masks(vpid, dbid):
    mod, tile, asic = velofactorize(int(vpid))

    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=control&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']

    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_mask_recipe.csv', delimiter = ',')
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255), 
               background_fill_color= '#FFFFFF', border_fill_color = '#FFFFFF')
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    c_mapper = LinearColorMapper(palette = 'Cividis256')
    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)

    _common_setup_mainplot(p)
    cb = r.construct_color_bar(width = 15, background_fill_color = '#FFFFFF')
    p.add_layout(cb, 'right')
    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch() )
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, width = 200)))

def get_bokeh_nstd(vpid, dbid, type, scan):
    mod, tile, asic = velofactorize(int(vpid))

    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=' + type + '&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']

    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_' + scan + '_' + 'std.csv', delimiter = ',')
    
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255))
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    c_mapper = LinearColorMapper(palette = 'Turbo256')

    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)
    cb_low = RangeSlider(start=0, end=40, value= (0,40), 
        step=0.25, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=140, x_range = (0,40), y_range = (0,6600))
    bins = np.linspace(-0, 40, 251)
    hist, edges = np.histogram(datamap, bins=bins)
    src_hist = ColumnDataSource(dict(x=edges[1:], y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist)
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'Noise sigma [DAC]'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = 0, 40
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 0, top = 6600, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)

    p.legend.padding = 1
    p.legend.spacing = 2
    _common_setup_mainplot(p)
    
    cb = r.construct_color_bar(width = 15)
    p.add_layout(cb, 'right')

    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch())
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

def get_bokeh_noisemap(vpid, dbid, type, scan, property, binsize = 5):
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=' + type + '&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']

    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_' + scan + '_' + property + '.csv', delimiter = ',')
    
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255))
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    low, high = np.round(np.min(datamap[np.nonzero(datamap)])/5)*5, np.round(np.max(datamap)/5)*5

    c_mapper = LinearColorMapper(palette = 'Turbo256', low = low, high = high)

    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)
    cb_low = RangeSlider(start= low, end= high, value= (low, high), 
        step=5, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=140, x_range = (low,high), y_range = (0,13000))
    graph.xaxis.ticker = np.linspace(low, high, 6)
    bins = np.linspace(low, high, int((high - low)/binsize) + 1)
    hist, edges = np.histogram(datamap, bins=bins)
    src_hist = ColumnDataSource(dict(x=edges[1:], y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist)
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'Noise baseline [DAC]'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = low, high
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 0, top = 13000, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)
    _common_setup_mainplot(p)
    
    cb = r.construct_color_bar(width = 15, title = 'Noise baseline [DAC]')
    p.add_layout(cb, 'right')

    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch())
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

def get_bokeh_trim(vpid, dbid):
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=normal&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']
    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_trim_recipe.csv', delimiter = ',')
    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255))
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    c_mapper = LinearColorMapper(palette = 'Turbo256')

    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)
    cb_low = RangeSlider(start=0, end=15, value= (0,15), 
        step=1, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=140, x_range = (0,15), y_range = (0,25000))
    bins = np.linspace(-0.5, 15.5, 16)
    hist, edges = np.histogram(datamap, bins=bins)
    src_hist = ColumnDataSource(dict(x=edges[1:]-0.5, y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist)
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'Noise sigma [DAC]'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = 0, 15
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 0, top = 25000, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)

    p.legend.padding = 1
    p.legend.spacing = 2
    _common_setup_mainplot(p)
    
    cb = r.construct_color_bar(width = 15)
    p.add_layout(cb, 'right')

    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch())
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

def get_bokeh_nstd_hist(vpid, dbid, type, scan):
    print('Moving in to nstd hist...')
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=' + type + '&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']

    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_' + scan + '_' + 'std.csv', delimiter = ',')
    
    f_src = 'linear' 
    p = figure(width=445, height=340, x_range=(0,40), y_axis_type = f_src)
    # Histogram
    bins = np.linspace(-0, 40, 201)
    hist_even, edges = np.histogram(datamap[:, ::2], bins=bins)
    hist_odd, edges = np.histogram(datamap[:, 1::2], bins=bins)
    hist_odd_16, edges = np.histogram(datamap[15::16, 1::2], bins=bins)

    src = ColumnDataSource(dict(x=edges[1:], y_even=hist_even, y_odd=hist_odd, y_odd_16 = hist_odd_16))
    p.step(x = 'x', y = 'y_even', line_color = '#0000ff', source = src, legend_label = "Even col.")
    p.step(x = 'x', y = 'y_odd', line_color = '#ff0000', source = src, legend_label = "Odd col.")
    p.step(x = 'x', y = 'y_odd_16', line_color = '#880000', source = src, legend_label = "Odd col. 16ths")
    _common_setup_mainplot(p)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p)) 

def get_bokeh_thrine(vpid, dbid):
    print('Moving in to thr in e hist...')
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    #fpath = _get('/api/equalisation/custom?type=control&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']
    eqid = dbid
    #print(eqid)
    print(eqid)
    fpath_baseline = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=path')['path']
    threshold = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=globalthreshold')['globalthreshold']
    fpath_baseline += 'Module' + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '_scan16_mean.csv'
    datamap_eq = np.genfromtxt(fpath_baseline, delimiter = ',').ravel() 
    print(datamap_eq, threshold)
    datamap_eq = (threshold - datamap_eq) * 1000 / 70
    print(datamap_eq)

    #datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
    #    '_VP' + str(tile) + '-' + str(asic) + '_scan16_' + 'std.csv', delimiter = ',')
    
    f_src = 'linear' 
    p = figure(width=445, height=340, x_range=(0,2000), y_axis_type = f_src)
    # Histogram
    bins = np.linspace(-0, 2000, 201)
    hist, edges = np.histogram(datamap_eq, bins=bins)
    #hist_odd, edges = np.histogram(datamap[:, 1::2], bins=bins)
    #hist_odd_16, edges = np.histogram(datamap[15::16, 1::2], bins=bins)

    src = ColumnDataSource(dict(x=edges[1:], y=hist))
    p.step(x = 'x', y = 'y', line_color = '#0000ff', source = src, legend_label = "Threshold per pixel", line_width = 1.5)
    #p.step(x = 'x', y = 'y_odd', line_color = '#ff0000', source = src, legend_label = "Odd col.")
    #p.step(x = 'x', y = 'y_odd_16', line_color = '#880000', source = src, legend_label = "Odd col. 16ths")
    #_common_setup_mainplot(p)
    p.legend.label_text_font_size = '9pt'
    p.xaxis.axis_label = 'Threshold [e]'
    p.xaxis.axis_label_text_font_size = '9pt'
    p.yaxis.axis_label = 'No. of pixels'
    p.yaxis.axis_label_text_font_size = '9pt'
    p.legend.padding = 1
    p.legend.spacing = 2
    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p)) 

def get_bokeh_thrine2d(vpid, dbid):
    print('Moving in to thr in e hist 2D...')
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    #fpath = _get('/api/equalisation/custom?type=control&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']
    eqid = dbid
    #print(eqid)
    print(eqid)
    fpath_baseline = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=path')['path']
    threshold = _get('/api/equalisation/custom?type=control&dbid=' +str(eqid)+ '&vpid=' +vpid+ '&param=globalthreshold')['globalthreshold']
    fpath_baseline += 'Module' + str(mod).zfill(2) + '_VP' + str(tile) + '-' + str(asic) + '_scan16_mean.csv'
    datamap_eq = np.genfromtxt(fpath_baseline, delimiter = ',')#.ravel() 
    print(datamap_eq, threshold)
    datamap_eq = (threshold - datamap_eq) * 1000 / 70
    print(datamap_eq)
    print(fpath_baseline, threshold)
    datamap = datamap_eq
    binsize = 2

    p = figure(width=445, height=340, x_range = (0,255), y_range = (0,255))
    sq_pitch = Slider(start=1, end=6, value=1, step=1, title="Square pitch", width = 160, show_value = False)
    
    src = ColumnDataSource(data = dict(image_now = [datamap], image_const = [datamap]))
    #low, high = np.round(np.min(datamap[np.nonzero(datamap)])/5)*5, np.round(np.max(datamap)/5)*5
    low, high = 500, 1500

    c_mapper = LinearColorMapper(palette = 'Viridis256', low = low, high = high)

    r = p.image(image = 'image_now', source = src, x = 0, y = 0, dw = 256, dh = 256, color_mapper = c_mapper)
    cb_low = RangeSlider(start= low, end= high, value= (low, high), 
        step=5, title="Colorbar scale", width = 160, show_value = True)

    cb_low.js_link('value', r.glyph.color_mapper, 'low', attr_selector=0)
    cb_low.js_link('value', r.glyph.color_mapper, 'high', attr_selector=1)

    graph = figure(width=220, height=140, x_range = (low,high), y_range = (0,2000))
    graph.xaxis.ticker = np.linspace(low, high, 6)
    bins = np.linspace(low, high, int((high - low)/binsize) + 1)
    hist, edges = np.histogram(datamap, bins=bins)
    src_hist = ColumnDataSource(dict(x=edges[1:], y=hist))
    graph.step(x = 'x', y = 'y', line_color = '#000000', source = src_hist)
    graph.yaxis.visible = False
    graph.xaxis.axis_label_text_font_size = '8pt'
    graph.xaxis.axis_label = 'Eff. threshold [e]'
    print(graph.x_range.start, graph.y_range.end)
    l_x, r_x = low, high
    l_src = ColumnDataSource(dict(l_x=[l_x], r_x = [r_x]))
    l_graph = graph.vspan(x = 'l_x', source = l_src, line_color = 'blue')
    r_graph = graph.vspan(x = 'r_x', source = l_src, line_color = 'red')
    l_box = Quad(left = 'l_x', right = 'r_x', bottom = 0, top = 2000, fill_color = 'blue', fill_alpha = 0.05)
    graph.add_glyph(l_src, l_box)
    callback_range = CustomJS(args=dict(source=l_src, cb = cb_low), code = _minihist_range())
    cb_low.js_on_change('value', callback_range)
    _common_setup_mainplot(p)
    
    cb = r.construct_color_bar(width = 15)
    p.add_layout(cb, 'right')

    callback = CustomJS(args=dict(source=src, sq_pitch = sq_pitch), code = _plot_and_pitch())
    sq_pitch.js_on_change('value', callback)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p, column(sq_pitch, cb_low, graph, width = 200)))

def get_bokeh_trim_hist(vpid, dbid):
    mod, tile, asic = velofactorize(int(vpid))
    #send GET request to the db
    fpath = _get('/api/equalisation/custom?type=normal&dbid=' +dbid+ '&vpid=' +vpid+ '&param=path')['path']

    datamap = np.loadtxt(fpath + 'Module' + str(mod).zfill(2) +  
        '_VP' + str(tile) + '-' + str(asic) + '_' + 'trim_recipe.csv', delimiter = ',')
    
    f_src = 'linear' 
    p = figure(width=445, height=340, x_range=(-0.5,15.5), y_axis_type = f_src)
    # Histogram
    bins = np.linspace(-0.5, 16.5, 18)
    hist_even, edges = np.histogram(datamap[:, ::2], bins=bins)
    hist_odd, edges = np.histogram(datamap[:, 1::2], bins=bins)
    hist_odd_16, edges = np.histogram(datamap[15::16, 1::2], bins=bins)
    
    src = ColumnDataSource(dict(x=edges[:-1] , y_even=hist_even, y_odd=hist_odd, y_odd_16 = hist_odd_16))
    p.step(x = 'x', y = 'y_even', line_color = '#0000ff', source = src, legend_label = "Even col.", mode = 'after')
    p.step(x = 'x', y = 'y_odd', line_color = '#ff0000', source = src, legend_label = "Odd col.", mode = 'after')
    p.step(x = 'x', y = 'y_odd_16', line_color = '#880000', source = src, legend_label = "Odd col. 16ths", mode = 'after')
    _common_setup_mainplot(p)

    output_file(filename='frontend/templates/main/bokeh.html', title='Static HTML file')
    path = save(row(p)) 
    
def logout(request):
    auth.logout(request)
    return redirect('/view/main')

def _common_setup_mainplot(p):
    p.legend.label_text_font_size = '8pt'
    p.xaxis.axis_label = 'Column ID'
    p.xaxis.axis_label_text_font_size = '8pt'
    p.yaxis.axis_label = 'Row ID'
    p.yaxis.axis_label_text_font_size = '8pt'
    p.legend.padding = 1
    p.legend.spacing = 2

def _plot_and_pitch():
    code = """                                        
            const R = sq_pitch.value;
            const squish = Math.pow(2, sq_pitch.value-1);
            var newsize = 256/squish;
            var image = source.data.image_now;
            for (let i=0; i<newsize; i++){
                for (let j=0; j<newsize; j++){
                    var xmulti = i*256*squish;
                    var ymulti = j*squish;
                    var getmean = 0;
                    for (let x=0; x<squish; x++){
                        for (let y=0; y<squish; y++){
                            getmean += source.data.image_const[0][xmulti + x*256 + ymulti + y];
                        }
                    }
                    getmean /= (squish*squish);
                    for (let x=0; x<squish; x++){
                        for (let y=0; y<squish; y++){
                            image[0][xmulti + x*256 + ymulti + y] = getmean;
                        }
                    }
                }
            }
            source.data.image = image; 
            source.change.emit();
            """
    return code

def _column_view():
    code =  """
            const id = cl_noise.value;
            var a = new Array(256);
            for (let i=0; i<256; i++){
                a[i] = datamap[id + i*256];
            }
            source.data.y = a;
            source.change.emit();
            """
    return code

def _minihist_range():
    code =  """                             
            source.data.l_x = [cb.value[0]];
            source.data.r_x = [cb.value[1]];
            source.change.emit(); 
            """
    return code
# the code for column slider view and plot
    #cl_noise = Slider(start=0, end=255, value=0, step=1, title="Column ID", width = 160, show_value = True)
    #cl_noise = NumericInput(value = 0, low = 0, high = 255, title = 'Col', height_policy = "fixed", 
    #                        sizing_mode = 'inherit', height = 5, width = 50)
    #cl_range = RangeSlider(start=0, end=255, value= (0,255),
    #    step=1, title="Row (pixel) range", width = 160, show_value = True)

    #gr_cl = figure(width=220, height=90, x_range = (0,255), y_range = (0,40))
    
    #src_cl = ColumnDataSource(dict(x=np.arange(256), y=datamap[:,0]))
    #gr_obj = gr_cl.step(x = 'x', y = 'y', line_color = '#000000', source = src_cl, mode='center')
    #gr_cl.xaxis.axis_label_text_font_size = '8pt'
    #gr_cl.xaxis.axis_label = 'Row (pixel) ID'

    #callback_col = CustomJS(args=dict(source=src_cl, cl_noise=cl_noise, datamap = datamap), code = _column_view())
    #cl_range.js_link('value', gr_cl.x_range, 'start', attr_selector=0)
    #cl_range.js_link('value', gr_cl.x_range, 'end', attr_selector=1)

    #cl_noise.js_on_change('value', callback_col)
