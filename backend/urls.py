from django.urls import path
from .views import main, logout, get_bokeh, getChipRecipe, get_saveset, get_data #recipe_table

urlpatterns = [
    path('main', main, name='main-view'),
    path('get_bokeh', get_bokeh, name='get_bokeh'),
    path('get_data', get_data, name='get_data'),
    path('get_saveset', get_saveset, name='get_saveset'),
    #path('recipe_table', recipe_table, name = 'recipe_table'),
    path('getChipRecipe', getChipRecipe, name = 'getChipRecipe'),
    path('logout', logout, name='logout')
]
