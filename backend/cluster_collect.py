import uproot
import numpy as np
import os
from datetime import datetime
import sys

def collect(run, criterion, setname, catalog):

    base2run = int(np.floor(run / 10000.0) * 10000)
    base3run = int(np.floor(run / 1000.0) * 1000)
    
    set = uproot.open(f'/hist/Savesets/ByRun/{setname}/{base2run}/{base3run}/VeloMon-run{run}.root')[catalog]#[entity]
    for mod in range(52):
        for sensor in range(4):
            name = f'VPClusterMapOnMod{mod}Sens{sensor}'
            #print(set[name].counts().shape)
            #print(set[name].counts())
            data = set[name].counts()
            #print(data)
            #print(np.count_nonzero(data > 50))
            #print(f'Module{mod}_Sensor{sensor} more than 50 hits in: {np.argwhere(data > 50)}')
            print(f'Module{mod}_Sensor{sensor}')
            to_mask = np.argwhere(data > criterion)
            srow, scol = '', ''
            for cord in to_mask:
                print(cord[0], cord[1])
                srow += f'{cord[1]} '
                scol += f'{cord[0]} '
            if srow != '':
                date = datetime.today().strftime('%d%m%Y')
                spath = '/group/velo/monitoring/scripts/noise/manualMasker.py'
                os.system(f'python {spath} --recipe DEFAULT --module {mod} --sensor {sensor} --rowPixels {srow} --colPixels {scol}')
                print(f'python {spath} --recipe DEFAULT --module {mod} --sensor {sensor} --rowPixels {srow} --colPixels {scol}')
                os.system(f'cp -r /calib/velo/masks/configuredMasks/singlePixels/M{str(mod).zfill(2)}_{date}/VeloPix/Matrix/DEFAULT/Module{str(mod).zfill(2)}/ /group/velo/config/VeloPix/Matrix/DEFAULT/')


        #print(i*3, np.sum(set[name].counts()[0:256]))
        #print(i*3 + 1, np.sum(set[name].counts()[256:512]))
        #print(i*3 + 2, np.sum(set[name].counts()[512:]))
        #print(set[name].to_numpy())
    #set = set.to_numpy()
    #print(set)
    #return set

if __name__ == '__main__':
    #date = datetime.today().strftime('%d%m%Y')
    if sys.argv[1] is not None and sys.argv[2] is not None:
        collect(int(sys.argv[1]), int(sys.argv[2]), 'VeloMon', 'VPClusterMonitors_NoBeam')
