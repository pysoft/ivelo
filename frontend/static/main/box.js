export const handleActivePageNumber = (currentPage) => {
    document.querySelectorAll(".pagination-number").forEach((button) => {
      button.classList.remove("active");
      const pageIndex = Number(button.getAttribute("page-index"));
      if (pageIndex == currentPage) {
        button.classList.add("active");
      }
    });
};

export const appendPageNumber = (index) => {
    const pageNumber = document.createElement("button");
    pageNumber.className = "pagination-number";
    pageNumber.innerHTML = index;
    pageNumber.setAttribute("page-index", index);
    const paginationNumbers = document.getElementById("pagination-numbers");
    paginationNumbers.appendChild(pageNumber);
  };

export const setCurrentPage = (pageNum) => {
    const pageLimit = 8;
    let currentPage = pageNum;

    handleActivePageNumber(pageNum);

    const prevRange = (pageNum - 1) * pageLimit;  
    const currRange = pageNum * pageLimit;           
    const paginatedList = document.getElementById("recipelist");
    const listItems = paginatedList.querySelectorAll("tr.apifilled");
    console.log(listItems);
    
    listItems.forEach((item, index) => {
      //console.log(item, index);       
      item.classList.add("hidden");              
      if (index >= prevRange && index < currRange) {              
        item.classList.remove("hidden");
        //console.log('unhide this');              
      }              
    });              
};

export const arrangePagination = (counter) => {
  const pageLimit = 8;
  const nrows = counter;
  const pageCount = Math.ceil(nrows / pageLimit);
  for (let i = 1; i <= pageCount; i++) {
    appendPageNumber(i);
  }
  setCurrentPage(1);
  console.log(document.querySelectorAll(".pagination-number"));
  document.querySelectorAll(".pagination-number").forEach((button) => {
      console.log('Clicked a button');
      const pageIndex = Number(button.getAttribute("page-index"));
      if (pageIndex) {
          button.addEventListener("click", () => {
              setCurrentPage(pageIndex);
          });
      }
  });
};

/*function getChipRecipeConfig(id_str, subid_str){
  const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
  $.ajax({
    url: 'http://127.0.0.1:8003/api/recipe/subrecipe/hw?id=' + id_str + '&subid=' + subid_str,
    type: 'GET',
    dataType: 'json',
    headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
    crossDomain: true,
    crossOrigin: true,
    success: function(data, textStatus) {
        var hwjson = JSON.parse(data[0].data);
        var select = document.getElementById('param_select');
        for (var a in select.options) { select.options.remove(0); }
        for (var item in hwjson) {
          var opt = document.createElement('option');
          opt.innerHTML = item;
          select.appendChild(opt);
        }
    },
    error: function(xhr, textStatus, errorThrown){
        console.log('fail');
    }
});
} */

export function buildbox(csrftoken, apiurl, which){
  //Solution inspired by Stack Overflow 15477527
  $.ajax({
    url: 'http://10.128.124.243:8003' + apiurl,
    type: 'GET',
    dataType: 'json',
    headers: {'X-CSRFToken':  csrftoken},
    crossDomain: true,
    crossOrigin: true,
    success: function(data, textStatus) {
        var boxfill = "<tr><th>ID</th><th>Name/Run</th><th>Date</th><th>Uploaded by</th><th>Uploaded from</th><th>Comment</th></tr>";
        let counter = 0;
        for (var item in data) {
            
            //if (counter == 0) boxfill += "<tr class = 'apifilled recipelist-selected'>";
            //else boxfill += "<tr class = 'apifilled'>";
            //if (counter != 0) 
            boxfill += "<tr class = 'apifilled'>";
            //else boxfill += "<tr class = 'apifilled recipelist-selected'>";

            var item_obj = data[item];
            //console.log(item_obj);
            for (var value in item_obj){
                if (value == 'path' || value == 'bxidref' || value == 'equalisation') continue;
                boxfill += " <td><div style='width:150px'> " + item_obj[value] + "</div> </td>";
            }
            //boxfill += "<td></td><td></td></tr>";
            counter++;
        }

        document.getElementById("recipelist").innerHTML = boxfill;
        //document.querySelectorAll('.recipelist tr').forEach( e => e.addEventListener("click", function() {
        //  if (which == 'hwrecipe') getChipRecipeConfig(e.childNodes[0].innerText, '0');
        //}));
        arrangePagination(counter);
        $(".recipelist tr").click(function(){
          console.log('debug');
          $(this).addClass('recipelist-selected').siblings().removeClass('recipelist-selected');
          //$(this).addClass('recipelist-selected').siblings().removeClass('recipelist-selected');
          //var event = new Event('change');
          // Dispatch it.
          //document.getElementById('param_select').dispatchEvent(event);
          document.getElementById('param_select').onchange();
        });
    },
});
};

export function clearmap(){
  var tablecells = document.querySelectorAll(".asiccell");
  for (var i = 0; i < tablecells.length; i++){
    tablecells[i].innerHTML = "";
    tablecells[i].style.removeProperty('background');
  }
};