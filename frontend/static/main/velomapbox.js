export class VeloMapBox
{
	constructor(velomap, infoboxbackgroundid, infoboxid)
	{
		this._velomap = velomap;
        this._infoboxid = infoboxid;
        this._infoboxbackgroundid = infoboxbackgroundid;
		this._asicName = 
		{
			"0": "VP0-0",
			"1": "VP0-1",
			"2": "VP0-2",
			"3": "VP1-0",
			"4": "VP1-1",
			"5": "VP1-2",
			"6": "VP2-0",
			"7": "VP2-1",
			"8": "VP2-2",
			"9": "VP3-0",
			"10": "VP3-1",
			"11": "VP3-2",
		}
    }

	Show(asicnumber)
	{
		//console.log('Here' + asicnumber);
		var asic = this._velomap.GetAsic(asicnumber);
		var module = Math.floor(asicnumber/12);
		var vp = this._asicName[asicnumber%12];
		document.getElementById("asicdetail").innerHTML = `
		<div class="asicdetailbox">
		<div style="color:white;font-family:sans-serif;position:relative;font-size:14px;
			left:20px;top:10px;"> Details for module ${module} ${vp}:</div>
		<div class='infobox'>
			<div class='infoboxentry'>
				ASIC ID: <span style="left:150px;position:absolute">${asicnumber}</span> </div>
			<div class='infoboxentry'>
				Global threshold: <span style="left:150px;position:absolute">${asic.Global_threshold}</span></div>
			<div class='infoboxentry'>
				Masked pixels: <span style="left:150px;position:absolute">${asic.Masked_pixels}</span> </div>
			<div class='infoboxentry'>
				Av. noise sigma: <span style="left:150px;position:absolute">-</span> </div>
			<div class='infoboxentry'>
				Trim resolution: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				Last calibrated: <span style="left:150px;position:absolute">-</span> </div>
			<div class='infoboxentry'><span>-</span></div>
			<div class='infoboxentry'>
				VFBK: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				VINCAS: <span style="left:150px;position:absolute">-</span> </div>
			<div class='infoboxentry'>
				VCASDISC: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				VPREAMP_CAS: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				IDISC: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				IKRUM: <span style="left:150px;position:absolute">-</span> </div>
			<div class='infoboxentry'>
				IPIXELDAC: <span style="left:150px;position:absolute">- </span></div>
			<div class='infoboxentry'>
				IPREAMP: <span style="left:150px;position:absolute">- </span></div>
		</div>`

	}
}
