import {
    VeloMapBox,
}
from "./velomapbox.js";
  

export class VeloMapBoard
{
	constructor(velomap, tableid, infoboxbackgroundid, infoboxid)
	{
		this._velomap = velomap;
        this._tableid = tableid;
		this._infobox = new VeloMapBox(velomap, infoboxbackgroundid, infoboxid);

		this._moduleClassMapping =
		{
			"A": "Ablock",
			"B": "Bblock",
			"C": "Cblock",
    	}

		this._asicName = 
		{
			"0": "VP0-0",
			"1": "VP0-1",
			"2": "VP0-2",
			"3": "VP1-0",
			"4": "VP1-1",
			"5": "VP1-2",
			"6": "VP2-0",
			"7": "VP2-1",
			"8": "VP2-2",
			"9": "VP3-0",
			"10": "VP3-1",
			"11": "VP3-2",
		}

	//this.bokeh_valid = ['masks', 'trim',
	//					'fine: nstd', 'fine: nmean', 'fine: nrate',
	//					'0x0: nstd', '0x0: nmean', '0x0: nrate',
	//					'0xF: nstd', '0xF: nmean', '0xF: nrate',
	//					'bxid_mean', 'bxid_std', 'bxid_min', 'bxid_max',
	//					'0x0: nstd (hist)', '0xF: nstd (hist)', 'fine: nstd(hist)',]

    this._assemble();
    this._setXaxisTicks();
    this._setYaxisTicks();
    this._fill();

		//THE CODE BELOW WAS FOR SCRIPTING TABS ONCE THEY WERE ABOVE THE DASHBOARD
		/*document.getElementById("superlink").addEventListener('click', event => {
			var tablink = document.getElementsByClassName("tablink");
			for (let i = 0; i < tablink.length; i++) {
			  tablink[i].className = tablink[i].className.replace(" active", "");
			}
			event.target.className += " active";
			console.log(event.target.className);
			
			let currentcell = null;
			console.log(event.target.textContent);
			for(let row = 0; row < this._velomap.rowcount; row++)
			{
				for(let column = 1; column <= this._velomap.columncount; column++)
				{
					let asic = (row) + (column -1) * 12;
					let asic_obj = this._velomap.GetAsic(asic);
					currentcell = document.querySelector(`[data-row='${row}'][data-column='${column}']`);
					currentcell.innerHTML = `
						<span class="module_quality">${asic_obj[this._click_map[event.target.textContent]]}</span><br />`;
				}
			}
		  this.GradeColor(event.target.textContent);
		})*/

		document.getElementById(this._tableid).addEventListener('click', event =>
		{
			let target = event.target;
			
			//TO HIGHLIGHT the tile
			if(target.parentElement.classList.contains("asiccell"))
			{
				target = event.target.parentElement;
			//	event.target.parentElement.className += " active";
			}
			//else{
			//	event.target.className += " active";
			//}

			if(target.classList.contains("asiccell"))
			{
				// this will clear the plots and exit if none object is clicked
				if (target.hasChildNodes() == false){
					if (document.getElementById("veloplot1").classList.contains("plotactive"))
						document.getElementById("bokeh1").innerHTML = "";
					else
						document.getElementById("bokeh2").innerHTML = "";
					return false;
				} 

				
				//this._infobox.Show(target.dataset.asicnumber);
				const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;//getCookie('csrftoken');

				//if (this.bokeh_valid.includes(
				//		document.getElementById("param_select").value) == false){
				//			return false;
				//	}
				
				try {
					var data = {'cat': document.querySelector(".subcattabactive").innerText,
								'vpid': target.dataset.asicnumber,
								'dbid': document.querySelector(".recipelist-selected").childNodes[1].innerText,
								'view': document.getElementById("param_select").value,
								'from': document.getElementById("param_equal1").value
					};
				} catch (error) {
					console.error(error);
					return null;
				}
				
				document.body.style.cursor='wait';

				console.log(document.querySelector(".subcattabactive").innerText);
				$.ajax({
					url:'./get_bokeh',
					type:'post',
					data: data,
					headers: {'X-CSRFToken':  csrftoken},
					credentials:'include',
					success : function(data){
					  if (document.getElementById("veloplot1").classList.contains("plotactive")){
					  	$('#bokeh1').html(data)
						document.getElementById('b1i2').selectedIndex = target.dataset.row;
						document.getElementById('b1i1').selectedIndex = target.dataset.asicnumber/12;
						let setvalue = document.getElementById('param_equal1').value +  " " +
							document.getElementById('param_select').value;
						let select_to_change = document.getElementById('b1i3');
						for(let i=0; i < select_to_change.length; i++) {
							if(select_to_change[i].innerHTML == setvalue) {
								select_to_change.selectedIndex = i;
								break;
							}
						}
					  }
					  else{
						$('#bokeh2').html(data)
						document.getElementById('b2i2').selectedIndex = target.dataset.row;
						document.getElementById('b2i1').selectedIndex = target.dataset.asicnumber/12;
						let setvalue = document.getElementById('param_equal1').value + " " +
							document.getElementById('param_select').value;
						let select_to_change = document.getElementById('b2i3');
						for(let i=0; i < select_to_change.length; i++) {
							if(select_to_change[i].innerHTML == setvalue) {
								select_to_change.selectedIndex = i;
								break;
							}
						}
					  }
					  document.body.style.cursor='auto';
					},
					error : function(xhr, status, error){
						console.log(error);
						document.body.style.cursor='auto';}
				});            
			}
		});
        
    }

	_assemble()
	{
		let table = document.getElementById(this._tableid);
		let currentcell;

		for(let row = 0; row <= this._velomap.rowcount; row++)
		{
            let newrow = document.createElement('tr');
			table.appendChild(newrow);
			if (row%3 == 2 && row != 0 && row != 11){
				table.appendChild(document.createElement('tr'));
				table.appendChild(document.createElement('tr')); 
			}
			
			for(let column = -1; column <= this._velomap.columncount; column++)
			{
				if (column  == 27){
					var cell = document.createElement('td');
					newrow.appendChild(cell);
					cell.style.height = '24px';
					cell.style.width = '3px';
				}
                var cell = document.createElement('td');

				cell.setAttribute("data-row", row);
				cell.setAttribute("data-column", column);
				
                newrow.appendChild(cell);
				cell.style.height = '24px';
				cell.style.width = '10px';
				currentcell = document.querySelector(`[data-row='${row}'][data-column='${column}']`);
				currentcell.classList.add("cell");
			}
		}
	}
	
	_setXaxisTicks()
	{
		for(let column = 1; column <= 26; column += 1)
		{
			let currentcell = document.querySelector(`[data-row='12'][data-column='${column}']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:10px">${(column - 1)*2}</span><br />`;
			currentcell.classList.add("headingcell");
		}
		for(let column = 27; column <= 52; column += 1)
		{
			let currentcell = document.querySelector(`[data-row='12'][data-column='${column}']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:10px">${(column-27)*2 + 1}</span><br />`;
			currentcell.classList.add("headingcell");
		}
	}

	_setYaxisTicks()
	{
		for(let row = 0; row < this._velomap.rowcount; row++)
		{
			let currentcell = document.querySelector(`[data-row='${row}'][data-column='-1']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:10px">${this._asicName[String(row)]}</span><br />`;
			currentcell.classList.add("headingcell");
		}
	}

    _fill()
    {
		let currentcell = null;
		let tooltip = "";
        

		for(let row = 0; row < this._velomap.rowcount; row++)
		{
            for(let column = 1; column <= this._velomap.columncount; column++)
            {
				let asic = (row) + (column -1) * 12;

				let columnhalf = column;
				if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
				else if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25

				let asic_obj = asic;//this._velomap.GetAsic(asic);
                currentcell = document.querySelector(`[data-row='${row}'][data-column='${columnhalf}']`);
                currentcell.setAttribute('data-asicnumber', asic_obj);

                currentcell.classList.add("asiccell");

				tooltip = `ASIC number: ${asic_obj}`;

		currentcell.setAttribute("title", tooltip.replace(/\t/g, ''));
            }
        }
            
	}
}
