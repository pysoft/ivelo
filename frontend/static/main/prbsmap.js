import {set_colorscale} from "./colorscale.js";

export const prbslinkmapping = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 13, 14, 15, 16, 17, 18 ,19, 20, 21]
export const prbsasicmapping = ['VP1-2', 'VP1-1', 'VP1-0', 'VP0-0', 'VP0-1', 'VP0-1', 'VP0-2', 'VP0-2',
								'VP0-2', 'VP0-2', 'VP2-2', 'VP2-1', 'VP2-1', 'VP2-0', 'VP2-0', 'VP2-0',
								'VP2-0', 'VP3-0', 'VP3-1', 'VP3-2']
//[5, 1, 3, 7, 9, 8, 6, 4, 2, 0, 13, 15, 17, 19, 21, 18, 20, 16, 14, 12];
//export const prbslinkmappingvp = ['VP0-0'];
//const rowbreaks = [0, 2, 6, 7, 8, 9, 13, 15, 16, 17, 18, 19]

export function assembleprbs(){
		let table = document.getElementById("prbsmap");
		let currentcell;

		for(let row = 0; row <= 20; row++)
		{
            let newrow = document.createElement('tr');
			table.appendChild(newrow);
			if (0){//(rowbreaks.includes(row)){
				table.appendChild(document.createElement('tr'));
				table.appendChild(document.createElement('tr')); 
			}
			
			for(let column = -1; column <= 52; column++)
			{
				if (column  == 27){
					var cell = document.createElement('td');
					newrow.appendChild(cell);
					cell.style.height = '13px';
					cell.style.width = '3px';
				}
                var cell = document.createElement('td');

				cell.setAttribute("prbs-row", row);
				cell.setAttribute("prbs-column", column);
				
                newrow.appendChild(cell);
				cell.style.height = '15px';
				cell.style.width = '10px';
				currentcell = document.querySelector(`[prbs-row='${row}'][prbs-column='${column}']`);
				currentcell.classList.add("cell");
			}
		}

		for(let column = 1; column <= 26; column += 1)
		{
			let currentcell = document.querySelector(`[prbs-row='20'][prbs-column='${column}']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:10px">${(column - 1)*2}</span><br />`;
			currentcell.classList.add("headingcell");
		}
		for(let column = 27; column <= 52; column += 1)
		{
			let currentcell = document.querySelector(`[prbs-row='20'][prbs-column='${column}']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:10px">${(column-27)*2 + 1}</span><br />`;
			currentcell.classList.add("headingcell");
		}
		for(let row = 0; row < 20; row++)
		{
			let currentcell = document.querySelector(`[prbs-row='${row}'][prbs-column='-1']`);
			currentcell.innerHTML = `<span class="module_quality" style = "font-size:8px">${prbslinkmapping[row] +"_" + prbsasicmapping[row]}</span><br />`;
			currentcell.classList.add("headingcell");
		}
	
		currentcell = null;
		let tooltip = "";
        
		for(let row = 0; row < 20; row++)
		{
            for(let column = 1; column <= 52; column++)
            {
				let columnhalf = column - 1;
                if ((column - 1) % 2 == 0) columnhalf = (column - 1)/2 + 1
                if ((column - 1) % 2 == 1) columnhalf = (column)/2 + 1 + 25

				let linkid = (row) + (column -1) * 20;
				let asic_obj = 1;
                currentcell = document.querySelector(`[prbs-row='${row}'][prbs-column='${columnhalf}']`);
                currentcell.setAttribute('data-asicnumber', asic_obj.Asic);

                //currentcell.innerHTML = `
                //    <span class="module_quality">${linkid}</span><br />`;
				//currentcell.style.background = jet(linkid/(1020/256));
                currentcell.classList.add("asiccell");

				tooltip = `ASIC number: ${asic_obj.Asic}
					Masked pixels: ${asic_obj.Masked_pixels}
					Global threshold: ${asic_obj.Global_threshold}`;

		currentcell.setAttribute("title", tooltip.replace(/\t/g, ''));
            }
        }
};
