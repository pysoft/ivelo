
export class VeloMap
{
	constructor()
	{
		this._rowcount = 12;
		this._columncount = 52;
		//this._data = calib_summary;
        this._FilterChangedEventHandlers = [];
    }

	get columncount() { return this._columncount };
	get rowcount() { return this._rowcount; };
	//get data() { return this._data; };

	GetAsic(asic){ return asic; }

}