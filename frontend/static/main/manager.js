import {buildrecipebox, fillchiprecipe} from "./recipebox.js";
import {buildmatrixrecipebox, fillmatrixrecipe} from "./matrixrecipebox.js";
import {buildequalisationbox, fillequalisation} from "./equalisationbox.js";
import {buildtabox, fillta} from "./tabox.js";
import {buildprbsbox, fillprbs} from "./prbsbox.js";
import {assembleprbs} from "./prbsmap.js"
import {builddacbox, filldac} from "./dacbox.js";
import {clearmap} from "./box.js";
import {bokehhandler} from "./bokehhandler.js"
import {available_cs, update_from_cs_settings} from "./colorscale.js";
import {buildrundb} from "./run.js";
import {runhandler} from "./runhandler.js";
import {buildrunnoise, fillrunnoise} from "./vpclusters.js";

window.addEventListener("load", function(event){
    buildrecipebox();
    //buildrundb();
    fillchiprecipe();

    document.getElementById("get_saveset_button").addEventListener("click", function() {
        runhandler();
    });

    document.getElementById("catlhcb").addEventListener("click", function() {
        clearmap();
        //document.getElementById("velomap")
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="none";
        document.getElementById("runcommand").style.display="block";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="305px";
        $("#prbsmap tr").remove();
        
        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catlhcb").classList.add('subcattabactive');
        $("#pagination-numbers button").remove();
        buildrundb("LHCb");
        fillchiprecipe();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("catvelo").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="none";
        document.getElementById("runcommand").style.display="block";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="240px";
        document.getElementById("browser").style.top="145px";
        document.getElementById("veloplots").style.display="none";
        document.getElementById("pagination-container").style.display="none";
        $("#prbsmap tr").remove();

        document.getElementById("runsearchbutton").addEventListener("click", function() {
            buildrundb("VELO", document.getElementById("runnumberinput").value);
        });
        
        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catvelo").classList.add('subcattabactive');
        $("#pagination-numbers button").remove();
        buildrundb("VELO", -1);
        fillchiprecipe();
        document.getElementById("param_equal_label").style.display="none";
    });


    document.getElementById("catut").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="none";
        document.getElementById("runcommand").style.display="block";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="305px";
        $("#prbsmap tr").remove();
        
        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catut").classList.add('subcattabactive');
        $("#pagination-numbers button").remove();
        buildrundb("UT");
        fillchiprecipe();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("runnoise").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();
        
        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("runnoise").classList.add('subcattabactive');
        $("#pagination-numbers button").remove();
        buildrunnoise();
        fillrunnoise();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("catchip").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();
        
        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catchip").classList.add('subcattabactive');
        $("#pagination-numbers button").remove();
        buildrecipebox();
        fillchiprecipe();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("catmatrix").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();

        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catmatrix").classList.add('subcattabactive');

        $("#pagination-numbers button").remove();
        buildmatrixrecipebox();
        fillmatrixrecipe();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("catequalisation").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();

        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catequalisation").classList.add('subcattabactive');

        $("#pagination-numbers button").remove();
        buildequalisationbox();
        //console.log("HERE", document.getElementById("recipelist").rows);
        //console.log("HERE", document.getElementById("recipelist").rows[0]);
        //document.getElemenyById("recipelist").childNodes()[0].addClass('recipelist-selected');
        //console.log($(".recipelist tr"))//trigger('onchange');
        fillequalisation();
        document.getElementById("param_equal_label").style.display="block";
    });

    document.getElementById("catta").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();

        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catta").classList.add('subcattabactive');

        $("#pagination-numbers button").remove();
        buildtabox();
        fillta();
        document.getElementById("param_equal_label").style.display="none";
    });

    document.getElementById("catprbs").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="none";
        document.getElementById("browser").style.height="235px";
        assembleprbs();

        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catprbs").classList.add('subcattabactive');

        $("#pagination-numbers button").remove();
        buildprbsbox();
        fillprbs();
        document.getElementById("param_equal_label").style.display="none";
    });
    
    document.getElementById("catdac").addEventListener("click", function() {
        clearmap();
        $("#recipelist tr").remove();
        document.getElementById("velomapdiv").style.display="block";
        document.getElementById("runcommand").style.display="none";
        document.getElementById("velomap").style.display="table";
        document.getElementById("browser").style.height="235px";
        $("#prbsmap tr").remove();

        document.querySelectorAll('.subcattab').forEach(el => {
            el.classList.remove('subcattabactive');
        });
        document.getElementById("catdac").classList.add('subcattabactive');

        $("#pagination-numbers button").remove();
        builddacbox();
        filldac();
        document.getElementById("param_equal_label").style.display="none";
    });
    document.getElementById("veloplots").addEventListener("click", event => {
        let target = event.target;
        console.log(target);
        document.getElementById("veloplot1").classList.remove("plotactive");
        document.getElementById("veloplot2").classList.remove("plotactive");
        document.getElementById("browser").style.height="235px";
        while (target.classList.contains("roundcorners") == false){
            target = target.parentElement;
        }
        target.classList.add("plotactive");
                
    });

    var elements = "";
    for (var i = 0; i < 52; i++) elements += "<option value='" + i + "'>Mod " + i + "</option>";
    document.getElementById("b1i1").innerHTML = elements;
    document.getElementById("b2i1").innerHTML = elements;

    elements = "";
    for (var i = 0; i < 4; i++){
        for (var j = 0; j < 3; j++){
            var id = 3*i + j;
            elements += "<option value='" + id + "'>VP" + i + "-" + j + "</option>";
        }
    }
    document.getElementById("b1i2").innerHTML = elements;
    document.getElementById("b2i2").innerHTML = elements;

    let bokeh_valid = ['control masks', 'thr_in_e', 'thr_in_e_2d',
                   'control fine: nstd', 'control fine: nmean', 'control fine: nrate',
                   'normal trim', 'normal trim (hist)',
                   'normal 0x0: nstd', 'normal 0x0: nmean', 'normal 0x0: nrate',
                   'normal 0xF: nstd', 'normal 0xF: nmean', 'normal 0xF: nrate',
                   'quick 0x0: nstd', 'quick 0x0: nmean', 'quick 0x0: nrate',
                   'quick 0xF: nstd', 'quick 0xF: nmean', 'quick 0xF: nrate',
                   'quick 0x0: nstd (hist)', 'quick 0xF: nstd (hist)', 
                   'normal 0x0: nstd (hist)', 'normal 0xF: nstd (hist)', 
                   'control fine: nstd (hist)', '_ incr. TA', '_ TA vs baseline',]
    
    elements = "";
    for (var i in bokeh_valid){
        elements += "<option value='" + bokeh_valid[i] + "'>" + bokeh_valid[i] + "</options>";
    }
    document.getElementById("b1i3").innerHTML = elements;
    document.getElementById("b2i3").innerHTML = elements;

    document.getElementById("b1ok").onclick = function() {bokehhandler($('#bokeh1'), 
                                                           document.getElementById("b1i1").value,
                                                           document.getElementById("b1i2").value,
                                                           document.getElementById("b1i3").value);};
    document.getElementById("b2ok").onclick = function() {bokehhandler($('#bokeh2'), 
                                                           document.getElementById("b2i1").value,
                                                           document.getElementById("b2i2").value,
                                                           document.getElementById("b2i3").value);};

    var cselect = document.getElementById("colorscale_select");
    elements = "";

    for (let cs in available_cs)
        elements += "<option value='" + available_cs[cs] + "'>" + available_cs[cs] + "</options>";
    cselect.innerHTML = elements;

    document.getElementById('colorscale_select').onchange = function() {update_from_cs_settings()};
    document.getElementById('colorscale_input1').onchange = function() {update_from_cs_settings()};
    document.getElementById('colorscale_input2').onchange = function() {update_from_cs_settings()};

}, false);

