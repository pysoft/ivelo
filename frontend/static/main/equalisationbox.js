import {buildbox, clearmap} from "./box.js";
import {getcolorrange} from "./colorrange.js";
import {set_colorscale} from "./colorscale.js";

export function fillequalisation(){
  console.log("Filling selects...");
  let scans = ['control', 'normal', 'quick'];
  let qparam = ['ipixeldac', 'rbottom0', 'rtop0', 'rbottomF', 'rtopF', 'dist', '0x0: bottom', '0x0: top', '0x0: step', 
                '0x0: nmean', '0x0: nstd',
                '0x0: nsplit', '0x0: nrate', '0xF: bottom', '0xF: top', '0xF: step', '0xF: nmean', '0xF: nstd',
                '0xF: nsplit', '0xF: nrate'];
  let nparam = ['trim', 'rbottom', 'rtop', 'dist', '0x0: bottom', '0x0: top', '0x0: step', '0x0: nmean', '0x0: nstd',
                '0x0: nsplit', '0x0: nrate', '0xF: bottom', '0xF: top', '0xF: step', '0xF: nmean', '0xF: nstd',
                '0xF: nsplit', '0xF: nrate'];
  let cparam = ['masks', 'globalthreshold', 'fine: bottom', 'fine: top', 'fine: step', 'fine: nmean', 'fine: nstd',
                'fine: nsplit', 'fine: nrate'];

  let i = 0;
  var select = document.getElementById('param_select');
  for (var a in select.options) { select.options.remove(0); }
  for (var param in cparam){
    var opt = document.createElement('option');
    opt.innerHTML = cparam[param];
    select.appendChild(opt);
  }

  i = 0;
  var select1 = document.getElementById('param_equal1');
  for (var a in select1.options) { select1.options.remove(0); }
  while (i < scans.length){
      var opt = document.createElement('option');
      opt.innerHTML = scans[i];
      select1.appendChild(opt);
      i++;
  }

  select1.onchange = function(){
    let param = [];
    let pselect1 = document.getElementById("param_equal1").value;
    if (pselect1 == 'control' ) param = cparam;
    else if (pselect1 == 'quick' ) param = qparam;
    else if (pselect1 == 'normal' ) param = nparam;


    let i = 0;
    var select = document.getElementById('param_select');
    for (var a in select.options) { select.options.remove(0); }
    while (i < param.length){
      var opt = document.createElement('option');
      opt.innerHTML = param[i];
      select.appendChild(opt);
      i++;
    }
  };
};

export function buildequalisationbox() {
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    buildbox(csrftoken, '/api/equalisation', 'equalisation');
    
    document.getElementById('param_select').onchange = function(){
      console.log('Will fill the dashboard.');
      var recipe_id = document.querySelectorAll(".recipelist-selected")[0].childNodes[1].innerText;
      var stype = document.getElementById("param_equal1").value;
      var param = document.getElementById("param_select").value;
      console.log(recipe_id);
      console.log(stype);

      var urlappend = '';
      if (param.includes('0x0: ')){urlappend = '&scan=scanlow'; param = param.replace('0x0: ', '');}
      if (param.includes('0xF: ')){urlappend = '&scan=scanhigh'; param = param.replace('0xF: ', '');}
      if (param.includes('fine: ')){urlappend = '&scan=scan'; param = param.replace('fine: ', '');}

      clearmap();
      
      $.ajax({
        url: 'http://10.128.124.243:8003/api/equalisation?id=' + recipe_id + '&type=' + stype + urlappend,// + '&subid=' + subid_str,
        type: 'GET',
        dataType: 'json',
        headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
        crossDomain: true,
        crossOrigin: true,
        success: function(data, textStatus) {
            //console.log('Success, filling in Select View', data);
            data.forEach((element) => {  
              let col = parseInt(element['asic']/12) + 1;
              let row = element['asic']%12;

              if ((col - 1) % 2 == 0) col = (col - 1)/2 + 1
              else if ((col - 1) % 2 == 1) col = (col)/2 + 1 + 25

              //console.log(element['asic'], row, col);
              var ccell = document.querySelector(`[data-row='${row}'][data-column='${col}']`);
              ccell.innerHTML = `<span class="module_quality">${element[param]}</span><br />`;
              var [low, high] = getcolorrange('equalisation', param);
              ccell.style.background = set_colorscale('jet', element[param], low, high, false);
            });
        },
        error: function(xhr, textStatus, errorThrown){
            console.log('fail');
        }
    });
    };

    //console.log("here... ", document.querySelectorAll(".recipelist-selected"));
    //document.getElementById('param_select').onchange();
    //console.log("here... ", document.querySelectorAll(".recipelist-selected"));
    //var cselect = document.getElementById("colorscale_select");
    //var opt = document.createElement('option');
    //opt.value = item;
    //opt.innerHTML = 'Jet';
    //cselect.appendChild(opt);
  }