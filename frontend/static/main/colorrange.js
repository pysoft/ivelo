let rangemap = {
    //'0x0: step': [0, 50],
    //'0x0: nstd': [0, 50],
    //'0x0: nsplit': [-2, 8],
    //'0xF: step': [0, 50],
    //'0xF: nstd': [0, 50],
    //'0xF: nsplit': [-2, 8],
    'trim': [0, 16],
    'masks': [0, 600],
    'step': [0, 40],
    'nstd': [10, 25],
    'nsplit': [-2, 8],
    'globalthreshold': [1500, 1800],
    'bottom': [1100, 1700],
    'top': [1400, 2100],
    'nmean': [1300, 1850],
    'nrate': [1350, 1900],
    'rbottom' : [1200, 1700],
    'rbottom0' : [1000, 1500],
    'rtop0' : [1500, 1900],
    'rbottomF' : [1100, 1700],
    'rtopF' : [1600, 2100],
    'rtop': [1500, 2100],
    'dist': [100, 240],
};

export function getcolorrange(page, param){
    //var low, high;
    if (page == 'equalisation'){
        if (param in rangemap) return rangemap[param];
    }
    return [0, 255];
}
