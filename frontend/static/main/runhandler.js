export function runhandler() {
	const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    //console.log(mod, asic, command);
	//try {
	//    var data = {'cat': document.querySelector(".subcattabactive").innerText,
	//				'vpid': mod * 12 + asic * 1,
	//				'dbid': document.querySelector(".recipelist-selected").childNodes[1].innerText,
	//				'view': command.replace(/[^ ]+ /, '' ),
	//				'from': command.replace(/ .*/,'')
	//	};
	//console.log(data);
	//} catch (error) {
	//	console.error(error);
	//	return null;
	//}
				
	//document.body.style.cursor='wait';
	var runid = document.querySelector(".recipelist-selected").childNodes[3].innerText;
	console.log('Run id should be this: ' + runid);

	console.log('Will call backend to read Savesets');
	$.ajax({
		url:'./get_saveset',
		type:'GET',
		data: {'runid': runid},
		headers: {'X-CSRFToken':  csrftoken},
		credentials:'include',
		success : function(){
			//if (document.getElementById("veloplot1").classList.contains("plotactive")){
			//canvas.html(data)
			//document.body.style.cursor='auto';
		},
		error : function(xhr, status, error){
			console.log(error);
			//document.body.style.cursor='auto';
        }
	});     
}