import {set_colorscale} from "./colorscale.js";
import {buildbox} from "./box.js";

export function filldac(){
    console.log("Filling selects...");
    var select = document.getElementById('param_select');
    for (var a in select.options) { select.options.remove(0); }
    let scans = ['None'];
    let i = 0;
    while (i < scans.length){
        var opt = document.createElement('option');
        //opt.value = item;
        opt.innerHTML = scans[i];
        select.appendChild(opt);
        i++;
    }
}

export function builddacbox() {
    const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
    buildbox(csrftoken, '/api/dac', 'ta');
    
    document.getElementById('param_select').onchange = function(){
      console.log('Will fill the dashboard.');
      var recipe_id = document.querySelectorAll(".recipelist-selected")[0].childNodes[1].innerText;
      var stype = document.getElementById("param_select").value;
      var min = 0;
      var max = 255;

      console.log(recipe_id);
      $.ajax({
        url: 'http://10.128.124.243:8003/api/dac?id=' + recipe_id + '&type=' + stype,// + '&subid=' + subid_str,
        type: 'GET',
        dataType: 'json',
        headers: {'X-CSRFToken':  csrftoken},//'http://localhost:8003/api/recipe/hw'},
        crossDomain: true,
        crossOrigin: true,
        success: function(data, textStatus) {
            console.log('Success, filling in Select View', data);
            for(let column = 1; column <= 52; column++)
            {
              for(let row = 0; row < 12; row++)
              {
                let asic = (row) + (column -1) * 12;
                var p = data[asic][stype];

                var currentcell = document.querySelector(`[data-row='${row}'][data-column='${column}']`);
                currentcell.innerHTML = `<span class="module_quality">${p}</span><br />`;
                currentcell.style.background = set_colorscale('jet', p, min, max);
              }
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log('fail');
        }
    });
    };
  }